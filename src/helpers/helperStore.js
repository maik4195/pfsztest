export const getDataAction = (requestCallback, propMutationName) => {
  return async (ctx) => {
    const { data } = await requestCallback();
    console.log(data);
    ctx.commit(propMutationName, data);
  };
};
