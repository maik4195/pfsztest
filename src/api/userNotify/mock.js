export default () => {
  return new Promise((res) => {
    setTimeout(() => {
      res({
        data: {
          notificationCount: Math.floor(Math.random() * (10 - 0 + 1)) + 0,
        },
      });
    }, 100);
  });
};
