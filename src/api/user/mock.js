export default () => {
  return new Promise((res) => {
    setTimeout(() => {
      res({ data });
    }, 1000);
  });
};

const data = {
  user: {
    name: "Моисеев Михаил Сергеевич",
    birthday: "21.04.1991",
    tabNumber: "ГОКИ 09000",
    position:
      "Электрослесарь (слесарь дежурный и по ремонту оборудования) I разряда",
  },
  progressInfo: {
    briefing: true,
    exam: false,
    testComplete: 100,
    maxTestComplete: 200,
    certificationDay: 30,
    lastCertificationDay: 365,
  },
};
