export default {
  setUser(state, data) {
    state.userInfo = data;
  },
  setUserNotify(state, data) {
    state.notification = data.notificationCount;
  },
  logOut(state) {
    state.userInfo = null;
  },
};
