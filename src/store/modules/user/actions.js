import getUser from "@/api/user/mock";
import getUserNotify from "@/api/userNotify/mock";
import { getDataAction } from "@/helpers/helperStore";

export default {
  getUser: getDataAction(getUser, "setUser"),
  getUserNotify: getDataAction(getUserNotify, "setUserNotify"),
  logOut(ctx) {
    ctx.commit("logOut");
  },
};
