import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";
import NotificationView from "@/views/NotificationView.vue";
import AchievementsView from "@/views/AchievementsView.vue";
import LearnView from "@/views/LearnView.vue";
import Login from "@/views/Login.vue";
import WorkView from "@/views/WorkView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/notification",
    name: "notify",
    component: NotificationView,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/work",
    name: "work",
    component: WorkView,
  },
  {
    path: "/achievments",
    name: "achievments",
    component: AchievementsView,
  },

  {
    path: "/learn",
    name: "learn",
    component: LearnView,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
